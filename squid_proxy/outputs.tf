output "public_ip_address" {
  description = "The actual ip address allocated to the resource."
  value       = data.azurerm_public_ip.vm.*.ip_address
}

output "vm_name" {
  description = "The actual name of the created VM."
  value       = azurerm_virtual_machine.vm.*.name
}

output "pip_vm_name_map" {
  description = "Return a map of every VM name and public IP created."
  value       = zipmap(azurerm_virtual_machine.vm.*.name, data.azurerm_public_ip.vm.*.ip_address)
}

output "created_resources" {
  description = "String version of created_resource.json file"
  value = [
    for proxy in azurerm_virtual_machine.vm.* : {
      name           = proxy.name
      public_ip      = data.azurerm_public_ip.vm[index(azurerm_virtual_machine.vm.*.name, proxy.name)].ip_address
      squid_username = var.randomize_user ? random_string.random_user.result : var.squid_username
      squid_password = var.randomize_password ? random_string.random_password.result : var.squid_password
      squid_port     = var.squid_port
    }

  ]
}

resource "local_file" "created_resources" {
  filename = "${var.vm_name}_out.json"
  content = jsonencode([
    for proxy in azurerm_virtual_machine.vm.* : {
      name           = proxy.name
      public_ip      = data.azurerm_public_ip.vm[index(azurerm_virtual_machine.vm.*.name, proxy.name)].ip_address
      squid_username = var.randomize_user ? random_string.random_user.result : var.squid_username
      squid_password = var.randomize_password ? random_string.random_password.result : var.squid_password
      squid_port     = var.squid_port
    }

  ])
}
