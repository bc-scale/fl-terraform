output "example_created_proxies" {
  description = "Map of created proxies with VM names and public ip addresses."
  value       = module.example.created_resources
}
